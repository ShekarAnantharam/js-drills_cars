// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


function getCarYears(inventory){

    let carYear=[];
    if (!Array.isArray(inventory)) {
        throw new Error("Invalid arguments")
    }
   else{
        for(car of inventory){
            carYear.push(car.car_year);
        }   
    }
    return carYear;
}

module.exports=getCarYears;