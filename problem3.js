// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function getCarModelsInAlphabetical(inventory){
    carModels=[];
    if (!Array.isArray(inventory)) {
        throw new Error("Invalid arguments")
    }
   else{
        for(car of inventory){
            carModels.push(car.car_model);
        }
    }
    return carModels;
}

module.exports=getCarModelsInAlphabetical;