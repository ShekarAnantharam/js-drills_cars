// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

//const getCarYears=require("/problem4");
const getCarYears=require("./problem4.js");
function getcarYearsBeforeReruiredYear(inventory,yearRequired){
    
   let carYears=getCarYears(inventory);
   
   let carsBeforeRequiredYear=[];
     if (!Array.isArray(inventory)) {
          throw new Error("Invalid arguments")
     }
     else{
   
          for (year of carYears){
      
                if(year<yearRequired){
    
                     carsBeforeRequiredYear.push(year);
    
               }
   
          }
     }
   
    return carsBeforeRequiredYear;
}
module.exports=getcarYearsBeforeReruiredYear;