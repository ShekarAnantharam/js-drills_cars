// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function getBMWAndAudi(inventory){
    let cars=[];
    if (!Array.isArray(inventory)) {
        throw new Error("Invalid arguments")
    }
    else{
        for(car of inventory){
            if(car.car_make==="BMW" || car.car_make==="Audi"){
                cars.push(car.car_make);
            }
        } 
        
    }return cars;
}

module.exports=getBMWAndAudi;
